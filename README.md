# Teeba-py

Ekika ky'omuzanyo gwa [Teeba][1] mu lulimi lwa Python.

Ebirimu:
- [ ] Ekitabo ky'ebigambo kisuka 500
- [ ] Kisobola okuyungula ebigambo ebikolwa byokka. 
- [ ] Kisobola okulondamu amannya gokka.

-----------------------------------


The python version of [Teeba][1].

Features:
- [ ] At least 500 Luganda words.
- [ ] Choice for verbs only.
- [ ] Choice for nouns only.


[1]: https://gitlab.com/pintert3/teeba
